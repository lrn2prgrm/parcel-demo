import { combineReducers, createStore } from "redux";
import Timer from "./Timer";

const rootReducer = combineReducers({
  time: Timer.reducer
});

function configureStore() {
  if (window.store == null) {
    window.store = createStore(rootReducer);
    return window.store;
  }
  if (NODE_ENV === "production") {
    return window.store;
  }
  //if (NODE_ENV === "development") {
  window.store.replaceReducer(rootReducer);
  return window.store;
  //}
} //todo learn to read from cross env

const store = configureStore();

//Optional action logs INIT
if (window.rawDispatch == null) {
  window.rawDispatch = store.dispatch;
}

store.dispatch = action => {
  console.group(action.type);
  console.log(store.getState());
  window.rawDispatch(action);
  console.log(action);
  console.log(store.getState());
  console.groupEnd(action.type);
};
//Optional action logs END

export default store;

import React from "react";

class Pure extends React.PureComponent {
  render() {
    return this.props.children == undefined ? null : this.props.children;
  }
}

export default Pure;

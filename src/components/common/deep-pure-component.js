import React from "react";

class DeepPureComponent extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.children !== nextProps.children;
  }
}

export default DeepPureComponent;

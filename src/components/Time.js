import React from "react";

export default function({ value = 0 }) {
  const negative = value < 0;
  value = negative ? value * -1 : value;
  const sign = negative ? "-" : "";
  let h = (value - value % 3600) / 3600;
  value = value - h * 3600;
  let m = (value - value % 60) / 60;
  value = value - m * 60;
  let s = value;
  if (h < 10) h = "0" + h;
  if (m < 10) m = "0" + m;
  if (s < 10) s = "0" + s;
  return <div>{sign + h + ":" + m + ":" + s}</div>;
}

import React from "react";

export default class Oscillator extends React.PureComponent {
  static defaultProps = {
    playing: true,
    pitch: 440,
    volume: 0.5
  };

  constructor(props, context) {
    super(props, context);
    this.o = ctx.createOscillator();
    this.g = ctx.createGain();
  }

  componentDidMount() {
    const { o, g } = this;
    const { playing, pitch, volume } = this.props;
    o.frequency.value = pitch;
    g.gain.value = volume;
    o.connect(g);
    g.connect(ctx.destination);
    o.start(0);
  }

  componentDidUpdate(prevProps, prevState) {
    this.doImperative();
  }

  componentWillUnmount() {
    this.o.stop(0);
  }

  doImperative = () => {
    const { o, g } = this;
    const { playing, pitch, volume } = this.props;
    if (playing) {
      o.frequency.value = pitch;
      g.gain.value = volume;
    } else {
      o.stop(0);
    }
  };

  render() {
    return null;
  }
}

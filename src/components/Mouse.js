import React from "react";

export default class Mouse extends React.Component {
  state = {
    x: 0,
    y: 0
  };

  m = e => this.setState({ x: e.clientX, y: e.clientY });

  render() {
    return <div onMouseMove={this.m}>{this.props.render(this.state)}</div>;
  }
}
